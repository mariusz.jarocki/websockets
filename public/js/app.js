var app = angular.module('app', ['ngCookies', 'ws'])

// websocket config
app.config(['wsProvider', function(wsProvider) {
    wsProvider.setUrl('ws://' + window.location.host)
}])

app.controller('Ctrl', [ '$http', '$cookies', 'ws', function($http, $cookies, ws) {
	
	var ctrl = this
	ctrl.session = $cookies.get('session')
	
	console.log('Controller starting, session:', ctrl.session)

	// handle messages from server
	ws.on('message', function(message) {
		try {
			ctrl.messages.push(JSON.parse(message.data))
		} catch(ex) {
			console.error('Message parse error')
		}
    })

	// initialize WS
    ws.send(JSON.stringify({ type: 'init', session: ctrl.session }))

	ctrl.messages = []
	ctrl.data = ''
	
	ctrl.get = function() {
		ws.send(JSON.stringify({ type: 'get' }))
	}
	
	ctrl.clear = function() {
		ctrl.messages.length = 0
	}
	
	ctrl.send = function() {
		if(ctrl.data) {
			$http.post('/send', ctrl.data).then(
				function(res) { console.log('Message sent to:', res.data.sentTo) },
				function(err) {}
			)
			ctrl.data = ''
		}
	}
	
}])