// builtin modules
var http = require('http')

// external modules
var static = require('node-static')
var cookies = require('cookies')
var uuid = require('uuid')
var ws = require('ws')

// global objects
var fileServer = new static.Server('./public')
var httpServer = http.createServer()
var wsServer = new ws.Server({ server: httpServer })

var sessions = {}

httpServer.on('request', function (req, res) {

    // maintain the existing session or create new one
    var appCookies = new cookies(req, res)
    var session = appCookies.get('session')
    var now = Date.now()
    if(!session || !sessions[session]) {
        session = uuid.v4()
        sessions[session] = { from: req.connection.remoteAddress, created: now, touched: now }
        console.log('New session', session, 'from', sessions[session].from)
    } else {
        sessions[session].touched = now
    }
    appCookies.set('session', session, { httpOnly: false })

	console.log('Session', session, req.method, req.url)
	
	switch(req.url) {
		case '/send':
			if(req.method != 'POST') {
				res.writeHead(405, 'Method not allowed', { 'Content-Type': 'text/plain' })
				res.end('Method not allowed')
				break
			}
			var message = { from: session }
			var data = ''
			req.setEncoding('utf8')
			req.on('data', function(chunk) { data += chunk; }).on('end', function() {
                message.data = data
                var sentTo = []
				wsServer.clients.forEach(function(client) {
					if(client.readyState == ws.OPEN && client.session != session) {
						console.log("Sending a message to client:", client.session, " with data:", message.data)
                        client.send(JSON.stringify(message))
                        sentTo.push(client.session)
					}
                })
                message.sentTo = sentTo
				res.writeHead(200, 'OK', { 'Content-Type': 'application/json' })
				res.end(JSON.stringify(message))
			})
			break
		default:
            fileServer.serve(req, res)
	}
}).listen(8888)

var n = 0

wsServer.on('connection', function connection(client) {
	
	console.log('Websocket connection established')
  
	client.on('message', function(message) {
		console.log('Websocket received:', message)
		
		var res = {}
		try {
			message = JSON.parse(message)
			switch(message.type) {
				case 'init':
					if(sessions[message.session]) {
						client.session = message.session
						sessions[message.session].wsClient = client
						res = { session: message.session, connected: true }
					} else {
						res = { session: message.session, connected: false, error: 'Session invalid' }
					}
					break
				case 'get':
                    n++
					res = { timestamp: (new Date()).toLocaleString(), data: 'some data #' + n }
					break
				default:
					res = { error: 'No type in message' }
			}
		} catch(err) {
            res = { error: JSON.stringify(err) }
            console.error('Message parse error')
		}
		client.send(JSON.stringify(res))
	})
 
})

console.log('Backend started')